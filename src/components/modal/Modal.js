/*
  Modal

  props :
  - children
  - onClose
  - onValidate
  - buttonLabel

  template :
  <div class="modalContainer" onClick="fermer()">
    <div class="modalContent" onClick="stop propagation()">
      <div>Contenu</div>

      <div class="buttonContainer">
        <Button onClick="valider()">Texte bouton</Button>
      </div>
    </div>
  </div>
*/
