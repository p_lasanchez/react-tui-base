/*
  chronoReducer

  utiliser la fonction utils/formatChrono

  state :
  - chrono
  - chronoValue (avec formatChrono)
  - isStarted
  - buttonValue
  
  actions :
  - INCREMENT
  - START
  - STOP
*/
