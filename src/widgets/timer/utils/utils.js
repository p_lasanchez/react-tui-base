export const formatTwoDigits = value => (value < 10 ? `0${value}` : `${value}`);

export const formatDate = date => {
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  return `${formatTwoDigits(hours)} : ${formatTwoDigits(
    minutes
  )} : ${formatTwoDigits(seconds)}`;
};

export const formatChrono = value => {
  const minutes = Math.floor(value / 60 / 100);
  const seconds = Math.floor((value - minutes * 60 * 100) / 100);
  const milli = value - minutes * 60 * 100 - seconds * 100;

  return `${formatTwoDigits(minutes)} : ${formatTwoDigits(
    seconds
  )} : ${formatTwoDigits(milli)}`;
};
