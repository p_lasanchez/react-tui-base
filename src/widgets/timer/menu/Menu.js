/*
  Menu

  props :
  - onSelect
  - selected

  template :
  <div class="menuContainer">
    <article
      onClick="click()"
      class="menuItem"
    >

      <icone fill="'darkorange' ou 'white'" />

      <span
        class="menuLabel selected"
      >
        label
      </span>

    </article>
  </div>
*/
