/*
  HeaderModal

  props :
  - onClose
  - onValidate
  - formerName

  utiliser useState

  template :
  <Modal onValidate="valider" onClose="close" buttonLabel="Valider">
    <input
      type="text"
      value="nom"
      onChange="change()"
      class="input"
      placeholder="Votre nom"
    />
  </Modal>
*/
