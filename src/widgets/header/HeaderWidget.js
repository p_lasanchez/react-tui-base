/*
  HeaderWidget

  Header qui affiche le nom utilisateur (stocké en localStorage)
  Si pas d'utilisateur stocké, ouvrir modale de saisie
  Au click sur bouton ouvrir modale de saisie

  utiliser useState

  utiliser useEffect

  utiliser localStorage.setItem et localStorage.getItem

  utiliser '../../assets/icons/account.svg'

  template :
  <article class="main">
  
    <button onClick="open()" class="button">
      <Account fill="white" />
    </button>

    <span>Welcome Toto</span>

    <HeaderModal
      onClose="close()"
      onValidate="validate()"
      formerName="nom ancien"
    />
    
  </article>
*/
