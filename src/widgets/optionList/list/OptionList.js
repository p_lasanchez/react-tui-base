/*
  OptionList

  Liste des options récupérées par http via redux
  
  utiliser useEffect

  utiliser useSelector
  utiliser useDispatch

  utiliser fetchOption
  utiliser setErrorMessage

  template :
  <section class="optionContainer">
    [
      <OptionItem option="option" />
    ]

    <Total />
  </section>
*/
