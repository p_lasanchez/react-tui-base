import React from 'react';
import { render } from 'react-dom';
import './css/styles.css';

import { PubWidget } from './widgets/pub';

// render PubWidget to element id 'pub-widget'
render(React.createElement(PubWidget), document.getElementById('pub-widget'));

// render HeaderWidget to element id 'header-widget'

// render TimerWidget to element id 'timer-widget'

// render OptionListWidget to element id 'options-widget'

// render CounterWidget to element id 'counter-widget'

// render ErrorWidget to element id 'error-widget'
